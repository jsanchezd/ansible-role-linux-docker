# Ansible Role Linux Docker

## Description

This is an Ansible Role that you can add to any playbook in order to install a standard setup of the Docker on Debian-based and RPM-based Linux distros.

Tested on CentOS 7 and Ubuntu 20.04.

## Dependencies

None

## Usage

In order to add this role on a playbook simply add it on the `roles` section or as an `include_role` directive inside one of the tasks files

#### Playbook example

```yaml
--- # Simple Docker playbook
- hosts: all
  become: yes
  become_method: sudo
  gather_facts: yes
  roles:
    - lx-docker
```

#### Tasks File example

```yaml
---
- name: Include Docker Role
  include_role:
    - name: lx-docker # or the name of the directory where you cloned this repository
```

#### Variables

One important variable you should have in mind is `docker['authorized_users']`. It allows to set a list of users to be added to the docker group in order to allow them using docker commands without sudo. 